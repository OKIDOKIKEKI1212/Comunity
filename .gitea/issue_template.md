<!-- NOTE: If your issue is a security concern, please send an email to contact@codeberg.org (and if related to Gitea also to security@gitea.io) instead of opening a public issue. Thank you.

Welcome to the Codeberg Community Tracker.
This is the right place for bug reports, feature requests and feedback. It's the central place where we track progress and discuss, so please open issues here unless you are sure it's directly related to a specific Codeberg product and only some contributors there need to join the discussion.
Easy rule: If you are unsure, report it here.

When reporting bugs or asking for features in the software itself, please understand that Codeberg is a fork of Gitea. Please always check upstream (→ see FAQ) if your there is already an open issue. If not, you'd really help us if you could directly get in touch with the maintainers and open an issue here if you think a wider audience should know about that (e. g. when discussing hotfixes, backports or when discussing whether some feature should become part of Gitea or a Codeberg "add-on").

If you don't have a GitHub account, please mention this and we'll gladly forward your report to the Gitea maintainers.

Thank you for reporting your findings and giving feedback on Codeberg.

## Some FAQ:

### What does upstream mean?
Upstream refers to Gitea, the software Codeberg is built upon. If we ask you if you can report upstream, please visit https://github.com/go-gitea/gitea/issues and check for the bug there and report elsewise. It's usually good if the person interested in a feature or bugfix opens the request to react to questions and join the discussion.
We would usually just fire the report, but won't find the time to properly react to that ...
**If you do not have a GitHub account**, just tell us and we'll happily forward the report for you.

### I just noticed a typo in the sign_up / sing_up route when regis...
No, this is not a typo, but intentional. It was a quick fix to avoid spammers targetting our instance and it actually worked out quite well to rename the route from sign_up to sing_up (few people notice, nice to see you have sharp eyes) ... we might have to take more effective countermeasures in the future, but for now we're actually quite good with that one ...

### How can I help?
If you want to help improving Codeberg as a community home for software development, we'll gladly welcome your contribution. Check out the docs about improving Codeberg https://docs.codeberg.org/improving-codeberg/ and have a look at the open issues, especially those that are looking for contribution https://codeberg.org/Codeberg/Community/issues?state=open&labels=105 - some of them don't even require much coding knowledge. We are also happy if you forward bug reports to Gitea if the original author hasn't done that yet or hasn't got a GitHub account.

-->
